https://web.facebook.com/Angels-Shelter-103800028789971

https://web.facebook.com/CapriislandVeganCafe


    field :address, :string
    field :avatar, :string
    field :city, :string
    field :content, :string
    field :country, :string
    field :email, :string
    field :name, :string
    field :phone, :string


    
      {{ with .Site.Data.faq}}
      <div class="col-lg-7 offset-lg-1">
        <div id="accordion" class="mt-5">
          {{ $.Scratch.Set "counter" 0 }}
          {{ range .faqItem }}
          {{ $.Scratch.Set "counter" (add ($.Scratch.Get "counter") 1) }}
          {{ "<!-- accordion item -->" | safeHTML }}
          <div class="card mb-1 rounded-0 border-0">
            <div class="card-header rounded-0 bg-white border p-0 border-0">
              <a class="card-link h4 d-flex tex-dark mb-0 p-2 justify-content-between" data-toggle="collapse"
                href="#collapse{{$.Scratch.Get `counter`}}">
                <span>{{ .title | markdownify }}</span> 
                <div class="ml-3"><i class="ti-plus h6 text-primary text-right border p-2 d-inline-block border-primary"></i></div>
              </a>
            </div>
            <div id="collapse{{$.Scratch.Get `counter`}}" class="collapse" data-parent="#accordion">
              <div class="p-2 content">{{ .content | markdownify }}</div>
            </div>
          </div>
          {{ end }}
        </div>
      </div>
      {{ end }}




      
            <button class="snipcart-add-item btn btn-sm btn-outline-primary" data-item-id="{{ .Params.ProductID }}"
              data-item-name="{{ .Title }}" {{ range first 1 .Params.Images }} data-item-image="{{ .image | absURL }}"
              {{ end }} data-item-price="{{ .Params.Price }}" data-item-url="{{ .Permalink }}">
              Add to cart
            </button>


            <button class="snipcart-add-item btn btn-sm btn-outline-primary" data-item-id="{{ .Params.ProductID }}"
              data-item-name="{{ .Title }}" {{ range first 1 .Params.Images }} data-item-image="{{ .image | absURL }}"
              {{ end }} data-item-price="{{ .Params.Price }}" data-item-url="{{ .Permalink }}">
              Add to cart
            </button>            

            <!--             <a class="text-color" href="tel:{{ .Site.Params.mobile }}"> -->


        <h6 class="font-weight-medium mb-4">Contact us</h6>
        <form action="{{ .Site.Params.contact.formAction }}" method="POST">
          <input type="text" class="form-control mb-2" id="name" name="name" placeholder="Your Name">
          <input type="email" class="form-control mb-2" id="email" name="email" placeholder="Your Email">
          <textarea name="message" id="message" class="form-control mb-2" placeholder="Your Message"></textarea>
          <button type="submit" value="send" class="btn btn-block btn-outline-primary rounded">Send Now</button>
        </form>            



      <div class="columns is-mobile is-multiline">
        {{ range .Params.pics }}

          <div class="column is-6-mobile is-3-desktop is-multiline">
            <div class="md__image">
              <img id="{{ first 6 (shuffle (seq 1 500)) }}" src="{{ . | safeURL }}" onclick="openModal(this.id)" alt="{{ . }}" />
            </div>
          </div>

        {{ end }}
      </div>


        

<div id="myModal" class="modal">
  <button class="modal-close" onclick="closeModal()">close</button>
  <div class="modal-content">
    <img class="modal-pic" id="modalPic" onclick="closeModal()" style="max-width: 100%; max-height: 80vh; margin: auto;">
  </div>
</div>

<script>
// Open the Modal
function openModal(clicked_id) {
  var src = document.getElementById(clicked_id).src;
  if (src.includes("#")) {
    src = src.substring(0, src.indexOf( "#" ));
  };
  document.getElementById("modalPic").src = src;
  document.getElementById("myModal").style.display = "block";
  // Ensures the site footer is not shown if front of the modal. Remove if this is not an issue or
  // there is no footer. "site-footer" id can also be changed appropriately.
  document.getElementById("site-footer").style.display = "hidden";
}

// Close the Modal
function closeModal() { 
  // prevents flashing last modal image while new id is loading on open
  document.getElementById("modalPic").src = "";
  document.getElementById("myModal").style.display = "none";
  // See note above regarding the footer 
  document.getElementById("site-footer").style.display = "block";
}
</script>

