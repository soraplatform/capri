---
title: "Brownie"
# date: 2022-04-17T11:22:16+06:00
description : "Brownie"
type: "menu"
cost: "Php 280"
# or 8 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "des/brownie.jpg"
---
