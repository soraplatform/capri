---
title: "Shawarma"
# date: 2022-04-17T11:22:16+06:00
description : "Our vegetarian Shawarma (/ʃəˈwɑːrmə/) uses vegetarian meat cut into thin slices and roasted."
type: "menu"
cost: "Php 95"
draft: true
# or 2.5 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/shawarma.jpg"
  - image: "s/shawarma1.jpg"  
---


 

<!-- , stacked in a cone-like shape, and roasted on a slowly-turning vertical rotisserie or spit. Originally made with lamb or mutton, it is now also made of chicken, turkey, beef, or veal.[5][6][1] Thin slices are shaved off the cooked surface as it continuously rotates.[7][8]  -->

Shawarma is one of the world's most popular street foods, especially in Egypt, Lebanon, Syria, the Caucasus, and the Middle East. It is an Arabic word derived from Turkish çevirme [tʃeviɾˈme] which means 'turning', referring to the turning rotisserie.

  
The shawarma technique of grilling a vertical stack of meat slices and cutting it off as it cooks first appeared in 19th-century Turkey as the doner kebab. Spices may include cumin, cardamom, cinnamon, turmeric and paprika.

<!-- [1][11][12] Both the Greek gyros and shawarma are derived from this.[1][2][13] Shawarma, in turn, led to the development during the early 20th century of the contemporary Mexican dish tacos al pastor when it was brought there by Lebanese immigrants.[2][14] -->

<!-- Shawarma is prepared from thin cuts of seasoned marinated lamb, mutton, veal, beef, chicken, or turkey. The slices are stacked on a skewer about 60 cm (20 in) high. Pieces of fat may be added to the stack to provide extra juiciness and flavor. A motorized spit slowly turns the stack of meat in front of an electric or gas-fired heating element, continuously roasting the outer layer. Shavings are cut off the rotating stack for serving, customarily with a long flat knife.[1]

, and in some areas baharat.[14][3] Shawarma is commonly served as a sandwich or wrap, in a flatbread such as pita or laffa.[1][15] In the Middle East, chicken shawarma is typically served with garlic sauce, fries, and pickles. In Syria and Lebanon, the garlic sauce that is served with the sandwich depends on the meat. The sauce known as "Toum" or "Toumie" which is made out of garlic, vegetable oil, lemon and egg white or starch is usually served with chicken Shawarma. On the other hand the sauce that is known as "Tarator" which is made out of garlic, Tahini sauce, Lemon and water is served with beef shawarma. -->