---
title: "Curry (or Peanut Butter) Tofu @ Php 220"
# date: 2022-04-17T11:22:16+06:00
description : "Made with Curry or Peanut Butter"
type: "menu"
cost: "Php 220"
# or 6 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# first image will be shown in the product page
images:
  - image: "s/curry.jpg"
---
