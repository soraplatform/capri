---
title: "Siopao @ Php 50"
# date: 2022-04-17T11:22:16+06:00
description : "Vegetarian Siopao"
type: "menu"
cost: "Php 50"
# or 2 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/siopao.jpg"
  - image: "s/siopao1.jpg"
  - image: "s/siopao2.jpg"
---


Our Siopao is made up of pao bun filled with a vegetarian meat. 