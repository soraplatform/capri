---
title: "Onigiri @ Php 115"
# date: 2022-04-17T11:22:16+06:00
description : "Our Onigiri is a Japanese dish With a Vegetarian Twist"
type: "menu"
cost: "Php 115"
# or 6 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/onigiri.jpg"
  - image: "s/onigiri1.jpg"
---
