---
title: "Organic Salad with Cheese or Mayo"
# date: 2022-04-17T11:22:16+06:00
description : "Fresh from our own garden"
type: "menu"
draft: true
cost: "Php 220"
# or 6 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/salad3.jpg"
  - image: "s/salad2.jpg"
  - image: "s/salad1.jpg"    
---
