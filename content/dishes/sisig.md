---
title: "Sisig @ Php 220"
# date: 2022-04-17T11:22:16+06:00
description : "Vegetarian Sisig"
type: "menu"
cost: "220 pesos"
# or 6 points
# costdef: "1 point = 1 value of 1 kilo rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/sisig.jpg"
---
