---
title: "Macaroni Cheese @ Php 150"
# date: 2022-04-17T11:22:16+06:00
description : "Vegetarian Cheese"
type: "menu"
cost: "Php 150"
#  or 6 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/maccheese.jpg"
  - image: "s/maccheese2.jpg"  
  # - image: "noimg.png"  
---

Our dish uses vegetarian cheese.