---
title: "Vegetarian Pizza Sausage @ Php 380"
# date: 2022-04-17T11:22:16+06:00
description : "Modern pizza was invented in Naples, and the dish and its variants have since become popular in many countries."
type: "menu"
cost: "Php 380"
# or 11 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/pizza.jpg"
---


Our pizzas are made up of a round, flat wheat-based dough topped with tomatoes, vegetarian cheese and sausage, mushrooms, and other vegetables, baked at a high temperature. 

<!-- The term pizza was first recorded in the 10th century in a Latin manuscript from the Southern Italian town of Gaeta in Lazio, on the border with Campania.[4]  -->

 

<!-- It has become one of the most popular foods in the world and a common fast food item in Europe, North America and Australasia; available at pizzerias (restaurants specializing in pizza), restaurants offering Mediterranean cuisine, via pizza delivery, and as street food.[5] Various food companies sell ready-baked pizzas, which may be frozen, in grocery stores, to be reheated in a home oven.

In 2017, the world pizza market was US$128 billion, and in the US it was $44 billion spread over 76,000 pizzerias.[6] Overall, 13% of the U.S. population aged 2 years and over consumed pizza on any given day.[7]

Raffaele Esposito is often considered to be the father of modern pizza
 -->