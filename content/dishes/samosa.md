---
title: "Indian Samosa @ Php 50 per piece"
# date: 2022-04-17T11:22:16+06:00
description : "A samosa (/səˈmoʊsə/) is a fried or baked pastry filled with spiced potatoes, onions, and peas."
type: "menu"
cost: "Php 50"
# or 2 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/samosa.jpg"
  - image: "s/samosa1.jpg"
---
 

<!-- It may take different forms, including triangular, cone, or half-moon shapes, depending on the region.[2][3][4] Samosas  -->

These are often accompanied by chutney and are a popular entrée, appetizer, or snack in South Asia, the Middle East, Central Asia, and East Africa.

<!-- The English word samosa derives from Hindi word 'samosa' (Hindi: समोसा),[5] traceable to the Middle Persian word sanbosag (سنبوسگ)[6] 'triangular pastry'.[7] Similar pastries are called sambusak in Arabic; Medieval Arabic recipe books sometimes spell it sambusaj.[8] The spelling samoosa is used in South Africa.[9][10]


The samosa has a Central Asian origin.[11][12] The earliest mention of the samosa was by Abbasid-era poet Ishaq al-Mawsili, praising the sanbusaj. Recipes are found in 10th–13th-century Arab cookery books, under the names sanbusak, sanbusaq, and sanbusaj, all deriving from the Persian word sanbosag. In Iran, the dish was popular until the 16th century, but by the 20th century, its popularity was restricted to certain provinces (such as the sambusas of Larestan).[2] Abolfazl Beyhaqi (995-1077), an Iranian historian, mentioned it in his history, Tarikh-e Beyhaghi.[13]

The Central Asian samsa was introduced to the Indian subcontinent in the 13th or 14th century by chefs from the Middle East and Central Asia who cooked in the royal kitchens for the rules of the Delhi Sultanate.[14] Amir Khusro (1253–1325), a scholar and the royal poet of the Delhi Sultanate, wrote in around 1300 CE that the princes and nobles enjoyed the "samosa prepared from meat, ghee, onion, and so on".[15] Ibn Battuta, a 14th-century traveler and explorer, describes a meal at the court of Muhammad bin Tughluq, where the samushak or sambusak, a small pie stuffed with minced meat, almonds, pistachios, walnuts and spices, was served before the third course, of pulao.[16] Nimatnama-i-Nasiruddin-Shahi, a medieval Indian cookbook started for Ghiyath Shah, the ruler of the Malwa Sultanate in central India, mentions the art of making samosa.[17] The Ain-i-Akbari, a 16th-century Mughal document, mentions the recipe for qottab, which it says, "the people of Hindustan call sanbúsah".[18] -->


Our samosa filling uses <!-- is prepared with an all-purpose flour (locally known as maida) and stuffed with a filling, often --> a mixture of diced or mashed boiled potato, onions, green peas, lentils, ginger, spices and green chili. 

<!-- A samosa can be vegetarian or non-vegetarian, depending on the filling. --> It is deep-fried in vegetable oil<!--  or rarely ghee to a golden brown. It --> and is served with fresh green chutney. <!-- , such as mint, coriander, or tamarind. --> 

<!-- It can also be prepared in a sweet form. Samosas are often served in chaat, along with the traditional accompaniments of either a chickpea or a white pea preparation, served with yogurt, tamarind paste and green chutney, garnished with chopped onions, coriander, and chaat masala. -->
