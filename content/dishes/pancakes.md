---
title: "Vegetarian Pancakes"
# date: 2022-04-17T11:22:16+06:00
description : "Vegetarian Pancakes"
type: "menu"
cost: "Php 70"
# or 6 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/pancake.jpg"
---

