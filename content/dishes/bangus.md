---
title: "Bangus Fillets"
# date: 2022-04-17T11:22:16+06:00
description : "Our Bangus Fillets are marinated in our special sauce"
type: "menu"
cost: "Php 360"
draft: true
# or 6 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/bangus.jpg"
---


<!-- Our Bangus Fillets are soft and --> 