---
title: "Bistek Tagalog @ Php 220"
# date: 2022-04-17T11:22:16+06:00
description : "Bistek is a Spanish loan word derived from the English words 'beef steak'"
type: "menu"
cost: "Php 220"
# or 6 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/bistek.jpg"
---

Bistek Tagalog is a traditional Filipino dish. <!-- also called Karne Frita in Ilonggo. --> It is made up of beef marinated in soy sauce with garlic and onion. 

We use vegetarian beef with the option to remove onion and garlic. 
 

<!--  Bistek (Spanish: bistec) or bistec is a  abbreviated. It has been widely speculated to have come into use in the early 19th century, along with its cousin in French for the concept of bifteck avec frites, according to Mon Dictionnaire de Cuisine.

Bistec encebollado is a Spanish dish that can be found in all former Spanish colonies, including the Caribbean islands of Cuba, Dominican Republic, and Puerto Rico.

Regional variations include: bistec de palomilla in Cuba, bistec ranchero in Mexico, bistec a caballo (topped with hogao and a fried egg) in Colombia, and bistec a lo pobre (served with fried plantain, fried eggs, fries, and rice) in Chile and Peru.

the dish was derived after the Spanish bistec encebollado
 -->


<!-- Bistek Tagalog is  meat dish originating from the Philippines. It consists of thinly sliced beef that's marinated and braised in a combination of citrus juice (usually from lemon, lime, or calamansi fruit), onions, garlic, soy sauce, and pepper.

It is believed that . When properly prepared, the flavors should be savory, salty, and tangy. It's important to use only the freshest beef and the most tender cuts such as top round and sirloin.

The dish is traditionally garnished with pan-fried onion rings on top and served with steamed rice on the side. 
 -->
