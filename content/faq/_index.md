---
title: "FAQ"
description : "FAQ for Capri Island Cafe"
---


<!-- ### What is the inspiration for Capri?



### How do I get to your cafe?

By car or taxi: 

By public transportation: 


### Do you deliver?
 -->


<!-- ### What are points and how do I pay in points?

Points are our version of barter. We give a service and you give us a service or product of the same value. That value is set in points. For example, we can give a yoga class for 40 points. You can render a service to us or give us a product of the same amount. This is useful in a high inflation scenario like what we have today.  

1 point = 1 kilo of NFA rice which is 35 pesos before rice tariffication was established. 


### Can I pay in cash? 

Yes! We accept both cash and points.

Just convert 1 point into 35 pesos. Therefore, 40 points = 1,400 pesos. If NFA rice goes up to 40 pesos per kilo, then 40 points becomes 1,600 pesos.   -->


## Do you have any advocacies?

Internationally, we advocate:
- Neo-humanism and environmentalism
- the Progressive Utilization of resources with the goal of providing minimum resources for all. This is the opposite of Neoclassical Economics which emphasizes maximum utility (via the profit maximization function) for the self.  
- the use of a non-monetary system called [Pantrypoints](https://pantrypoints.com) as an implementation of Progressive Utilization


<!-- Locally, we advocate:
- changing the name of the Philippines into [Maharlika](https://superphyics.org/maharlikanism)
- the promotion of Maharlikan culture such as arnis, the virtues of honesty and hardwork, and Maharlikan food such as [vegetable sinigang](https://superphyics.org/maharlika/culture/war-adobo) instead of pork adobo or Western fastfood
- repeal of neoliberal policies such as rice tariffication, privatization, etc.  -->
