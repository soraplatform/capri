---
title: "Thai Milk Tea @ Php 95"
# date: 2022-04-17T11:22:16+06:00
description : "Thai Milk Tea"
type: "menu"
cost: "Php 95"
# or 2.5 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "d/thai.jpg"
---
