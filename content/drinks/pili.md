---
title: "Chocolate Pili Nut Shake @ Php 150"
# date: 2022-04-17T11:22:16+06:00
description : "Chocolate Pili Nut Shake"
type: "menu"
cost: "Php 150"
# or 4 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/pili2.jpg"
---
