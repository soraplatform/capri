---
title: "Soya Milk @ Php 80"
# date: 2022-04-17T11:22:16+06:00
description : "Soya Milk"
type: "menu"
cost: "Php 80"
# or 2 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "d/soyamilk2.jpg"
---
