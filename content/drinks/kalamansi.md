---
title: "Kalamansi Tea @ Php 95"
# date: 2022-04-17T11:22:16+06:00
description : ""
type: "menu"
cost: "Php 95"
# or 6 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# draft: true
images:
  - image: "d/kalamansi.jpg"
---
