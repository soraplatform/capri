---
title: "Barako Coffee @ Php 100"
# date: 2022-04-17T11:22:16+06:00
description : "Barako coffee was introduced to the Philippines in the 1740s by Spanish friars, planted in Lipa, Batangas."
type: "menu"
cost: "Php 100"
# or 3 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "d/coffee.jpg"
  - image: "d/coffeebeans.jpg"  
---


Kapeng barako or Batangas coffee, is a coffee variety grown in Batangas and Cavite. Barako in the languages of the Philippines means "stud", and is associated with the image of masculinity. 

Barako has a strong flavor and fragrance reminiscent of aniseed.

Its taste is said to be superior to Robusta, and most Philippine coffee drinkers prefer barako to Arabica. Arabica–varraco and excelsa–varraco blends are popular and create a cup with broader flavor range. It has a distinctive flavor and a strong fragrance reminiscent of aniseed.

<!-- It belongs to the species Coffea liberica. The term is also used to refer to all coffee coming from those provinces. 

Barako trees are some of the largest commercially cultivated coffee trees, which make them more difficult to grow. They are considered endangered due to low production and demand. It is listed in the Ark of Taste international catalogue of endangered heritage foods by the Slow Food movement.[1]
 -->


<!-- Barako in Philippine languages is equivalent to the English term "stud" (both literally and figuratively), from Spanish varraco, "wild boar" (baboy ramo in Tagalog). The word is associated with connotations of masculinity and machismo in Filipino culture.[2]

History
 Barako was also historically exported from the 1860s onwards to San Francisco in the United States and to parts of Europe, commanding prices of five times the prices of other Asian coffee beans. In 1876, barako cultivation spread to the neighboring province of Cavite.[3] -->

<!-- The Philippines became one of the top four producers of coffee in the world in the 1880s, after coffee rust devastated plantations worldwide. However, in 1889, the coffee industry in the Philippines also collapsed after coffee rust spread to the islands. 

This caused most farmers to shift to other crops. Only a few barako seedlings survived, most of which were now in Cavite. In the mid-20th century, coffee demand once again surged, but barako didn't make a comeback because of the difficulty of cultivating it in comparison to other coffee varieties. Instead the new surge in coffee production focused on coffee rust-resistant cultivars imported from the United States.[3][4]


Barako Coffee uses liberica beans

It is asymmetric, with one side shorter than the other side, creating characteristic "hook" at the tip. The central furrow is also more jagged in comparison to other coffee beans.
 -->



<!--  from Mindoro, Philippines
The shape of the liberica beans is unique among other commercial species (arabica, robusta, and excelsa). [5] -->

<!-- Barako trees are very tall, reaching up to 20 m (66 ft) high. They are harvested using ladders. The size of the cherries, the beans, and the leaves of barako are also among the largest of all coffee varieties.[5][6]
 -->
