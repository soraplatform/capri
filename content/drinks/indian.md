---
title: "Indian Milk Tea"
# date: 2022-04-17T11:22:16+06:00
description : "Milk tea refers to several forms of beverage found in many cultures, containing some combination of tea and milk."
type: "menu"
cost: "Php 80"
# or 2 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "d/milktea.jpg"
---

Our milk tea is based on Masala chai, also known as masala tea. It is a spiced milk tea drunk in India. 
<!-- Irani chai, a type of milk tea made with pure milk mixed with mawa, prepared in Iranian-style cafes in Hyderabad, India -->

 

<!-- Beverages vary based on the amount of each of these key ingredients, the method of preparation, and the inclusion of other ingredients (varying from sugar or honey to salt or cardamom).[1] Instant milk tea powder is a mass-produced product.[2

British tea, served with milk
Bubble tea, also known as pearl milk tea or boba milk tea, a Taiwanese tea-based drink invented in Taichung in the 1980s
Burmese-style milk tea, called laphet yay cho (လက်ဖက်ရည်ချို), made with strongly brewed black tea leaves, and sweetened with a customized ratio of condensed milk and evaporated milk, is popular in Myanmar.[3][4] It is commonly served in tea shops, which first emerged during British rule in Burma.[5][6]
Cambric tea, a sweetened hot-milk beverage, often made with a small amount of tea[7]
Hong Kong-style milk tea, black tea sweetened with evaporated milk originating from the days of British colonial rule in Hong Kong
Doodh pati chai, literally 'milk and tea leaves', a tea beverage drunk in India, Pakistan, Nepal, and Bangladesh

Teh tarik, a kind of milk tea popular in Malaysia and Singapore

Suutei tsai, a salty Mongolian milk tea
Shahi haleeb, a Yemeni milk tea served after chewing qat


Thai tea, a sweet tea-based drink popular in Southeast Asia
Royal milk tea, The tea company Lipton invented product in 1965 as part of its “royal” recipe series in Japan[8]
Dalgona milk tea, milk tea sweetened with traditional Korean dalgona, a honeycomb-like toffee[9]
It is believed that Thailand consumes up to six cups of bubble tea per person per month. Malaysia, Singapore, Vietnam and Indonesia consume three cups per person per month.[10]

In Britain, when hot tea and cold milk are drunk together, the drink is simply known as tea due to the vast majority of tea being consumed in such a way. The term milk tea is unused, although one may specify tea with milk if context requires it. This may cause confusion for people from cultures that traditionally drink tea without milk.
 -->