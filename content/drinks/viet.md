---
title: "Vietnamese Caphe @ Php 95"
# date: 2022-04-17T11:22:16+06:00
description : "Vietnamese iced coffee or cà phê đá is a traditional Vietnamese coffee recipe."
type: "menu"
cost: "Php 95"
# or 2.5 points
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "d/viet.jpg"
---


Cà phê đá is made using medium to coarse ground dark roast Vietnamese-grown coffee with a small metal Vietnamese drip filter (phin cà phê). After the hot water is added, the drip filter releases drops of hot coffee slowly into a cup. 

This finished cup of hot coffee is then quickly poured into a glass full of ice making the finished Vietnamese iced coffee.

A popular way to drink Vietnamese coffee is cà phê sữa đá, which is Vietnamese iced coffee mixed with sweetened condensed milk. 

<!-- This is done by putting two to three teaspoons or more of sweetened condensed milk into the cup prior to the drip filter process.[1]

Coffee was introduced into Vietnam in 1857 by a French Catholic priest in the form of a single Coffea arabica tree.[2] The beverage was adopted with regional variations. Because of limitations on the availability of fresh milk, as the dairy farming industry was still in its infancy,[3] the French and Vietnamese began to use sweetened condensed milk with a dark roast coffee.

Vietnam did not become a major exporter of coffee until the Đổi Mới reforms and opening of the economy after the war. Now, many coffee farms exist across the central highlands. Vietnam is now the largest producer of the Robusta variety of coffee and the second largest producer of coffee worldwide. -->