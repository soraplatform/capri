---
title: "Plant-based Cooking Workshop"
date: 2022-05-01T11:22:16+06:00
# meta description
description : "We have a Plant-based Cooking Workshop This May 1"
type: "events"
# first image will be shown in the product page
images:
  - image: "e/cooking.jpg"
---
